## INTRODUCTION

The CKEditor 5 DeepL module integrates DeepL into CKEditor 5.

It provides a button to translate texts within the CKEditor 5 editor
using the DeepL API. Just select the text you want to translate and click
the DeepL button. Depending on your DeepL API key (Pro or Free), you will
have a different options in a CKEditor dialog.

The module also provides a "deepl translation block" which you can place in any region of your admin-theme,
so you have your deepl-translator on every admin-page as well.

This module can also display usage statistics for every key which are received
from the DeepL API.

## REQUIREMENTS

ckeditor5_deepl requires the following modules:

- [key](https://www.drupal.org/project/key)

And the following libraries:

- [deeplcom/deepl-php](https://github.com/DeepLcom/deepl-php)

Both of these are included in the `composer.json` file and will be installed,
else make sure to install them manually.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

Using composer is recommended though.

## CONFIGURATION

After installing the module, follow these steps to configure it.
Each drupal editor format using CKEditor 5 can be configured separately.

- Create a key at /admin/config/system/keys with type 'DeepL API Key'
- Edit a CKEditor text-format configuration at /admin/config/content/formats
- Drag the 'DeepL' button to the active toolbar
- A Tab "Deepl Integration" will appear in the Ckeditor5 plugin settings with further configuration.

## MAINTAINERS

- Hendrik Kuck (uniquename) - https://www.drupal.org/u/uniquename
- Sascha Meissner (sascha_meissner) - https://www.drupal.org/u/sascha_meissner

