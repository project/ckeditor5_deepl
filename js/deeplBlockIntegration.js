(function (Drupal, drupalSettings, once) {

  'use strict';

  /**
   * Adds a toggle button to DeeplBlockIntegration.
   */
  Drupal.behaviors.deeplBlockIntegration = {
    attach: function (context, settings) {
      let $deeplBlock = once('deeplBlockIntegration', '.block-ckeditor5-deepl', context);
      if (!$deeplBlock[0]) {
        return;
      }
      $deeplBlock = $deeplBlock[0];

      // Add toggle button.
      if (!document.getElementById('deepl_toggle_button')) {
        var button_html = '<div id="deepl_toggle_button" class="deepl_toggle_button" onclick="Drupal.behaviors.deeplBlockIntegration.toggle()"></div>';
        document.body.insertAdjacentHTML('beforeend', button_html);
      }
    },
    toggle: function() {
      let $deeplBlock = document.getElementsByClassName('block-ckeditor5-deepl')[0];
      $deeplBlock.classList.toggle('hidden');
    }

  };

})(Drupal, drupalSettings, once);
