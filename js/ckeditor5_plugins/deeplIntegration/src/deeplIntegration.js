
import deeplIntegrationUI from "./deeplIntegrationUI";
import DeepIntegrationRequest from "./deeplIntegrationApi";
import { Plugin } from "ckeditor5/src/core";
import { ContextualBalloon } from "ckeditor5/src/ui";

export default class deeplIntegration extends Plugin {

  static get requires() {
    return [deeplIntegrationUI, DeepIntegrationRequest, ContextualBalloon];
  }
}
