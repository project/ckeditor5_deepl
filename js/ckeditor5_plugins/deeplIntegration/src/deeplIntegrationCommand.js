import { Command } from "ckeditor5/src/core";
import FormView from "./deeplIntegrationForm";
import { ContextualBalloon, clickOutsideHandler } from "ckeditor5/src/ui";
import DeepIntegrationRequest from "./deeplIntegrationApi";

export default class DeepIntegrationCommand extends Command {
  constructor(editor, config) {
    super(editor);
    this._balloon = this.editor.plugins.get(ContextualBalloon);
    this.formView = this._createFormView();
    this._config = config;
    this._request = this.editor.plugins.get(DeepIntegrationRequest);
  }

  execute(options = {}) {
    this._showUI();
  }

  refresh() {
    const { model } = this.editor;
    const { selection } = model.document;
    // Determine if the selection contains text.
    const range = selection.getFirstRange();
    let selectedText = false;
    for (const item of range.getItems()) {
      if (item.data) {
        selectedText = true;
        break;
      }
    }
    // Disable command if no text is selected.
    this.isEnabled = selectedText ? true : false;
  }

  _createFormView() {
    const editor = this.editor;
    const formView = new FormView(editor);

    this.listenTo(formView, "submit", () => {

      let sHtmlSelection = this.editor.data.stringify(
        this.editor.model.getSelectedContent(
          this.editor.model.document.selection
        )
      );

      let target_langcode = this.formView.languageDropdownView.buttonView.id;
      if (!target_langcode) {
        return;
      }

      this._hideUI();

      this._request.doRequest("api/ckeditor-deepl/translate", {
        text: sHtmlSelection,
        target_langcode: target_langcode,
        format_id: editor.sourceElement.getAttribute(
          "data-editor-active-text-format"
        ),
        options: this._config.deepl,
      });
    });

    // Hide the form view after clicking the "Cancel" button.
    this.listenTo(formView, "cancel", () => {
      this._hideUI();
    });

    // Hide the form view when clicking outside the balloon.
    clickOutsideHandler({
      emitter: formView,
      activator: () => this._balloon.visibleView === formView,
      contextElements: [this._balloon.view.element],
      callback: () => this._hideUI(),
    });

    return formView;
  }

  _getBalloonPositionData() {
    const view = this.editor.editing.view;
    const viewDocument = view.document;
    let target = null;

    // Set a target position by converting view selection range to DOM.
    target = () =>
      view.domConverter.viewRangeToDom(viewDocument.selection.getFirstRange());

    return {
      target,
    };
  }

  _showUI() {
    this._balloon.add({
      view: this.formView,
      position: this._getBalloonPositionData(),
    });

    this.formView.focus();
  }

  _hideUI() {
    this.formView.element.reset();
    this._balloon.remove(this.formView);
    this.editor.editing.view.focus();
  }
}
