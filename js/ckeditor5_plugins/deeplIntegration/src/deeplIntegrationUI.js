import { Plugin } from "ckeditor5/src/core";
import { ButtonView } from "ckeditor5/src/ui";
import icon from "../../../../icons/deepl.svg";
import DeeplIntegrationCommand from "./deeplIntegrationCommand";

export default class deeplIntegrationUI extends Plugin {
  init() {
    const editor = this.editor;
    const config = this.editor.config.get("ckeditor5_deepl_integration");

    editor.commands.add(
      "deeplIntegrationCommand",
      new DeeplIntegrationCommand(editor, config)
    );

    // This will register the deepl toolbar button.
    editor.ui.componentFactory.add("deeplIntegration", (locale) => {

      const buttonView = new ButtonView(locale);
      const command = editor.commands.get("deeplIntegrationCommand");

      // Create the toolbar button.
      buttonView.set({
        label: editor.t("DeepL"),
        icon,
        tooltip: true,
      });

      // Bind the state of the button to the command.
      buttonView.bind("isOn", "isEnabled").to(command, "value", "isEnabled");

      // Execute the command when the button is clicked (executed).
      this.listenTo(buttonView, "execute", () =>
        editor.execute("deeplIntegrationCommand")
      );

      return buttonView;
    });
  }
}
