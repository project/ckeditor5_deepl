import { Plugin } from "ckeditor5/src/core";

export default class DeepIntegrationRequest extends Plugin {
  constructor(editor) {
    super(editor);
  }

  doRequest(endpoint, data) {
    const editor = this.editor;

    editor.model.change(async (writer) => {
      const url = drupalSettings.path.baseUrl + endpoint;

      this.toggleLoader();

      this.getTranslation(url, data).then((result) => {
        const text = result.text;
        const viewFragment = this.editor.data.processor.toView(text);
        const modelFragment = this.editor.data.toModel(viewFragment);

        editor.model.insertContent(modelFragment);
        this.toggleLoader();
      });
    });
  }

  async getTranslation(url, data) {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      credentials: "same-origin",
      body: JSON.stringify(data),
    });

    if (!response.ok) {
      const message = `An error has occured: ${response.status}`;
      throw new Error(message);
    }

    return await response.json();
  }

  toggleLoader() {
    let loader = document.querySelector(".ajax-progress--fullscreen");
    if (loader) {
      loader.parentNode.removeChild(loader);
    } else {
      let body = document.querySelector("body");
      body.insertAdjacentHTML(
        "afterend",
        Drupal.theme.ajaxProgressIndicatorFullscreen()
      );
    }
  }
}
