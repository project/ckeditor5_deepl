import {
  View,
  LabeledFieldView,
  LabelView,
  createLabeledInputText,
  createDropdown,
  Model,
  addListToDropdown,
  ButtonView,
  submitHandler,
  FormHeaderView
} from "ckeditor5/src/ui";
import { Collection } from 'ckeditor5/src/utils';
import { icons } from "ckeditor5/src/core";
import icon from "../../../../icons/deepl.svg";

export default class FormView extends View {
  constructor(editor) {
    super(editor.locale);
    this._config = editor.config.get("ckeditor5_deepl_integration");

    this.formHeader = new FormHeaderView(editor.locale, {label: Drupal.t("Deepl"), icon: icon})

    this.languageDropdownLabelView = new LabelView(editor.locale);
    this.languageDropdownLabelView.set({
      text: 'Target language'
    });

    this.languageDropdownView = this._createDropdown(editor.locale);

    const items = new Collection();
    const targetLanguages = this._config.deepl.target_languages;
    for (const [key, value] of Object.entries(targetLanguages)) {
      items.add({
        type: "button",
        model: new Model({
          id: key,
          withText: true,
          label: value,
        }),
      });
    }

    // @todo: Find a better way to match drupal language code with deepl language code.
    const currentLanguageKey = drupalSettings.path.currentLanguage.toUpperCase();
    if (targetLanguages.hasOwnProperty(currentLanguageKey)) {
      this.languageDropdownView.buttonView.set({
        id: currentLanguageKey,
        label: targetLanguages[currentLanguageKey],
        withText: true,
      });
    } else {
      this.languageDropdownView.buttonView.set({
        label: "Target language",
        withText: true,
      });
    }

    this.languageDropdownView.on('execute', (eventInfo) => {
      const { id, label } = eventInfo.source;
      this.languageDropdownView.buttonView.set( {
        id: id,
        label: label,
        withText: true
      });
    });

    // Create a dropdown with a list inside the panel.
    addListToDropdown( this.languageDropdownView, items );

    // Add formality dropdown if enabled.
    if (this._config.deepl.show_formality) {
      this.formalityDropdownLabelView = new LabelView(editor.locale);
      this.formalityDropdownLabelView.set({
        text: 'Formality'
      });

      this.formalityDropdownView = this._createDropdown(editor.locale);

      const formalityItems = new Collection();
      const formalities = this._config.deepl.formalities;

      for (const [key, value] of Object.entries(formalities)) {
        formalityItems.add({
          type: "button",
          model: new Model({
            id: key,
            withText: true,
            label: value,
          }),
        });
      }

      const formality = this._config.deepl.formality;
      if (formalities.hasOwnProperty(formality)) {
        this.formalityDropdownView.buttonView.set({
          id: formality,
          label: formalities[formality],
          withText: true,
        });
      } else {
        this.formalityDropdownView.buttonView.set({
          id: "default",
          label: "default",
          withText: true,
        });
      }

      this.formalityDropdownView.on('execute', (eventInfo) => {
        const { id, label } = eventInfo.source;
        this.formalityDropdownView.buttonView.set( {
          id: id,
          label: label,
          withText: true
        });
      });

      // Create a dropdown with a list inside the panel.
      addListToDropdown( this.formalityDropdownView, formalityItems );
    }

    this.cancelButtonView = this._createButton(
      "Cancel",
      icons.cancel,
      "ck-button-cancel"
    );
    this.cancelButtonView.delegate("execute").to(this, "cancel");


    this.saveButtonView = new ButtonView();

    this.saveButtonView.set({
      label: Drupal.t("Translate"),
      withText: true,
      icon: icons.check,
      class: "ck-button-save",
    });
    this.saveButtonView.type = "submit";

    let views = [];

    views.push(this.formHeader);
    views.push(this.languageDropdownLabelView);
    views.push(this.languageDropdownView);

    if (this._config.deepl.show_formality) {
      views.push(this.formalityDropdownLabelView);
      views.push(this.formalityDropdownView);
    }

    views.push(this.saveButtonView);
    views.push(this.cancelButtonView);


    this.childViews = this.createCollection(views);

    this.setTemplate({
      tag: "form",
      attributes: {
        class: ["ck", "ck-deepl-translation-form", "ck-deepl-form"],
        tabindex: "-1",
      },
      children: this.childViews,
    });
  }

  _createDropdown(locale, label) {
    const dropdown = new createDropdown(locale);
    dropdown.set({
        label: label,
        withText: true
    });
    return dropdown;
  }

  _createInput(label) {
    const labeledInput = new LabeledFieldView(
      this.locale,
      createLabeledInputText
    );
    labeledInput.label = label;
    return labeledInput;
  }

  _createButton(label, icon, className) {
    const button = new ButtonView();

    button.set({
      label,
      icon,
      tooltip: true,
      class: className,
    });

    return button;
  }

  render() {
    super.render();

    // Submit the form when the user clicked the save button
    // or pressed enter in the input.
    submitHandler({
      view: this,
    });
  }

  focus() {

  }
}
