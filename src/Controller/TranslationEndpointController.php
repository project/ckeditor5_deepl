<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_deepl\Controller;

use DeepL\TextResult;
use Drupal\ckeditor5_deepl\DeeplKeys;
use Drupal\ckeditor5_deepl\DeeplTranslator;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for CKEditor 5 deepl routes.
 */
final class TranslationEndpointController extends ControllerBase {

  /**
   * The controller constructor.
   */
  public function __construct(
    private readonly DeeplTranslator $deeplTranslator,
    private readonly DeeplKeys $deeplKeys,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('ckeditor5_deepl.translator'),
      $container->get('ckeditor5_deepl.keys'),
    );
  }

  /**
   * Builds the response.
   */
  public function __invoke(Request $request) {

    $payload = json_decode($request->getContent());

    $translation = $this->translateRequest($payload);

    $response = new JsonResponse($translation, 200, []);
    return $response;
  }

  /**
   * Sends a request from ckeditor to the Deepl API.
   *
   * @param mixed $data
   *   The data from ckeditor plugin.
   *
   * @return \DeepL\TextResult
   *   The response from Deepl API.
   */
  public function translateRequest($data): TextResult {
    $response = [];
    // @todo Add a try catch block to handle exceptions.
    $editor_settings = $this->loadEditorDeeplSettings($data->format_id);

    $api_key = $this->deeplKeys->loadApiKey($editor_settings['api_key']);
    $source_langcode = !empty($data->source_lang) ? $data->source_lang : NULL;
    $target_langcode = $data->target_langcode;
    $options = [
      'split_sentences' => $editor_settings['split_sentences'],
      'preserve_formatting' => $editor_settings['preserve_formatting'],
      'tag_handling' => $editor_settings['tag_handling'],
    ];

    // If its a free API-key, the formality option is not available.
    // See https://support.deepl.com/hc/en-us/articles/4406433229330-The-formal-informal-feature-isn-t-displayed
    if (!$this->deeplTranslator->isAuthKeyFreeAccount($api_key)) {
      $options['formality'] = $data->formality;
    }

    $response = $this->deeplTranslator->translate($api_key, $data->text, $source_langcode, $target_langcode, $options);

    return $response;
  }

  /**
   * Loads the editor deepl settings.
   *
   * @param string $format_id
   *   The format id.
   *
   * @return array
   *   The editor deepl settings.
   */
  private function loadEditorDeeplSettings($format_id) {
    /** @var \Drupal\editor\Entity\Editor $editor */
    $editor = $this->entityTypeManager()->getStorage('editor')->load($format_id);
    $settings = $editor->getSettings();
    return $settings['plugins']['ckeditor5_deepl_integration']['deepl'];
  }

}
