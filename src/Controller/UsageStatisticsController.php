<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_deepl\Controller;

use Drupal\ckeditor5_deepl\DeeplTranslator;
use Drupal\Core\Controller\ControllerBase;
use Drupal\key\KeyRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for CKEditor 5 deepl routes.
 */
final class UsageStatisticsController extends ControllerBase {

  /**
   * The controller constructor.
   */
  public function __construct(
    private readonly DeeplTranslator $deeplTranslator,
    private readonly KeyRepository $keyRepository,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('ckeditor5_deepl.translator'),
      $container->get('key.repository'),
    );
  }

  /**
   * Builds the response.
   */
  public function __invoke(): array {

    $keys = $this->keyRepository->getKeysByType('deepl_api_key');

    $build = [];
    foreach ($keys as $key) {
      $key_id = $key->id();
      $key_value = $key->getKeyValue();
      $usage = $this->deeplTranslator->getUsageStatistics($key_value);
      $free_key = $this->deeplTranslator->isAuthKeyFreeAccount($key_value);

      $build[$key_id] = [
        '#type' => 'details',
        '#title' => $key->label(),
        '#description' => $free_key ? $this->t('Free key', [], ["context" => "ckeditor5_deepl"]) : $this->t('Paid key', [], ["context" => "ckeditor5_deepl"]),
        '#open' => TRUE,
        '#collapsible' => FALSE,
      ];

      $build[$key_id]['usage'] = [
        '#type' => 'item',
        '#markup' => nl2br($usage->__toString()),
      ];

      /*
      $header = [
      'type' => $this->t('Type', [], ["context" => "ckeditor5_deepl"]),
      'count' => $this->t('Count', [], ["context" => "ckeditor5_deepl"]),
      'limit' => $this->t('Limit', [], ["context" => "ckeditor5_deepl"]),
      ];

      $build[$key_id]['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => [],
      ];*/
    }

    return $build;
  }

}
