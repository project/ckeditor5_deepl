<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_deepl;

use Drupal\key\Entity\Key;
use Drupal\key\KeyRepositoryInterface;

/**
 * Deepl keys.
 */
final class DeeplKeys {

  /**
   * Constructs a DeeplKeys object.
   */
  public function __construct(
    private readonly KeyRepositoryInterface $keyRepository,
  ) {}

  /**
   * Loads the key from the key repository.
   *
   * @param string $key_id
   *   The key id.
   *
   * @return string|null
   *   The key entity or null.
   */
  public function loadApiKey($key_id): string|null {
    $key = $this->keyRepository->getKey($key_id);
    return !is_null($key) ? $key->getKeyValue() : NULL;
  }

  /**
   * Returns the first Deepl API key found or NULL.
   *
   * @return \Drupal\key\Entity\Key|null
   *   The first Deepl API key found or NULL.
   */
  public function loadOneDeeplKey(): Key|null {
    $keys = $this->keyRepository->getKeysByType('deepl_api_key');
    foreach ($keys as $key) {
      if ($key->getKeyValue()) {
        return $key;
      }
    }
    return NULL;
  }

}
