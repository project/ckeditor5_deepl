<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_deepl\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use JsonSchema\Validator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

/**
 * Checks if passed parameter matches the route configuration.
 *
 * Usage example:
 * @code
 * foo.example:
 *   path: '/example/{parameter}'
 *   defaults:
 *     _title: 'Example'
 *     _controller: '\Drupal\ckeditor5_deepl\Controller\Ckeditor5DeeplController'
 *   requirements:
 *     _translate_access: 'TRUE'
 * @endcode
 */
final class TranslationEndpointAccessChecker implements AccessInterface {

  /**
   * Constructs a FooAccessChecker object.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * Access callback.
   *
   * @DCG
   * Drupal does some magic when resolving arguments for this callback. Make
   * sure the parameter name matches the name of the placeholder defined in the
   * route, and it is of the same type.
   * The following additional parameters are resolved automatically.
   *   - \Drupal\Core\Routing\RouteMatchInterface
   *   - \Drupal\Core\Session\AccountInterface
   *   - \Symfony\Component\HttpFoundation\Request
   *   - \Symfony\Component\Routing\Route
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account = NULL, Request $request = NULL): AccessResult {

    // Check if the request has a JSON content type.
    if ($request->headers->get('Content-Type') !== 'application/json') {
      return AccessResult::forbidden('Invalid request.');
    }

    // Check if the request has a valid JSON payload.
    $payload = json_decode($request->getContent());
    if (empty($payload) || !is_object($payload)) {
      return AccessResult::forbidden('Invalid request.');
    }

    // Validate the request data.
    $validator = new Validator();
    $validator->check($payload, $this->getRequestDataSchema());

    if (!$validator->isValid()) {
      return AccessResult::forbidden('Invalid request.');
    }

    // Check if the user has permission to use the text format.
    $format_id = $payload->format_id;
    if ($account->hasPermission('use text format ' . $format_id)) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden('Invalid request.');
  }

  /**
   * Returns the schema for the request data.
   *
   * @return array
   *   The schema for the request data.
   */
  private function getRequestDataSchema() {
    return (object) [
      'type' => 'object',
      'properties' => (object) [
        'text' => (object) ['type' => 'string'],
        'format_id' => (object) ['type' => 'string'],
        'source_langcode' => (object) ['type' => 'string'],
        'target_langcode' => (object) ['type' => 'string'],
        'formality' => (object) ['type' => 'string'],
      ],
      'required' => ['text', 'format_id', 'target_langcode'],
    ];
  }

}
