<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_deepl;

use DeepL\TextResult;
use DeepL\Translator;
use DeepL\Usage;

/**
 * Deepl translator.
 */
final class DeeplTranslator {

  /**
   * The Deepl translator.
   *
   * @var \DeepL\Translator
   */
  protected $translator;

  /**
   * Constructs a TranslationService object.
   */
  public function __construct(
    private readonly DeeplKeys $deeplKeys,
  ) {}

  /**
   * Get a preconfigured instance of the Deepl translator.
   */
  private function translator($api_key = NULL, $reset = FALSE): Translator {
    if ($this->translator && !$reset) {
      return $this->translator;
    }

    if (!$api_key && $reset) {
      throw new \InvalidArgumentException('No API key provided.');
    }

    $this->translator = new Translator($api_key);

    return $this->translator;
  }

  /**
   * Calls the Deepl API to translate the text.
   *
   * @param string $api_key
   *   The Deepl API key.
   * @param string $text
   *   The text to translate.
   * @param string $source_lang
   *   The source language.
   * @param string $target_lang
   *   The target language.
   * @param array $options
   *   The translation options. See \DeepL\TranslateTextOptions.
   *
   * @return \DeepL\TextResult
   *   The translation result.
   */
  public function translate($api_key, $text, $source_lang, $target_lang, $options): TextResult {
    return $this->translator($api_key)->translateText($text, $source_lang, $target_lang, $options);
  }

  /**
   * Checks if the API key is for a free account.
   *
   * @param string $api_key
   *   The Deepl API key.
   *
   * @return bool
   *   TRUE if the API key is for a free account, FALSE otherwise.
   */
  public function isAuthKeyFreeAccount($api_key): bool {
    return $this->translator($api_key)->isAuthKeyFreeAccount($api_key);
  }

  /**
   * Get the usage statistics for the API key.
   *
   * @param string $api_key
   *   The Deepl API key.
   *
   * @return \DeepL\Usage
   *   The usage statistics.
   */
  public function getUsageStatistics($api_key): Usage {
    return $this->translator($api_key, TRUE)->getUsage();
  }

  /**
   * Get either source or target languages.
   *
   * @param string $type
   *   The languages type.
   */
  public function getLanguages($type = 'source') {
    $deepl_api_key = $this->deeplKeys->loadOneDeeplKey();

    if (!$deepl_api_key->getKeyValue()) {
      return [];
    }

    if ($type === 'source') {
      $languages = $this->translator($deepl_api_key->getKeyValue())->getSourceLanguages();
    }
    else {
      $languages = $this->translator($deepl_api_key->getKeyValue())->getTargetLanguages();
    }

    return $languages;
  }

}
