<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_deepl;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Deepl helper class.
 */
final class DeeplHelper {

  use StringTranslationTrait;

  /**
   * Constructs a DeeplHelper object.
   */
  public function __construct(
    private readonly DeeplTranslator $translator,
    private readonly CacheBackendInterface $cache,
  ) {}

  /**
   * Check if the given key is a valid Deepl key.
   *
   * @param string $key
   *   The key to check.
   *
   * @return bool
   *   TRUE if the key is valid, FALSE otherwise.
   */
  public function isValidDeeplKey(string $key): bool {
    try {
      $this->translator->getUsageStatistics($key);
      return TRUE;
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

  /**
   * Get supported remote languages as form options.
   */
  public function getSupportedRemoteLanguagesOptions($type = 'source'): array {
    $languages = $this->getSupportedRemoteLanguages($type);
    $options = [];
    foreach ($languages as $language) {
      $options[$language->code] = $this->t($language->name, [], ['context' => 'ckeditor5_deepl']);
    }
    return $options;
  }

  /**
   * Returns the array of languages supported by the Deepl API.
   */
  public function getSupportedRemoteLanguages($type = 'source'): array {
    $cacheId = 'ckeditor5_deepl:supported_remote_languages:' . $type;

    if ($cache = $this->cache->get($cacheId)) {
      return $cache->data;
    }

    if ($languages = $this->translator->getLanguages($type)) {
      $this->cache->set($cacheId, $languages, Cache::PERMANENT, ['deepl_api']);
      return $languages;
    }

    return [];
  }

  /**
   * Get supported formalities as form options.
   */
  public function getSupportedFormalities(): array {
    return [
      'default' => $this->t('default'),
      'more' => $this->t('more - for a more formal language'),
      'less' => $this->t('less - for a more informal language'),
      'prefer_more' => $this->t('prefer_more - for a more formal language if available, otherwise fallback to default formality'),
      'prefer_less' => $this->t('prefer_less - for a more informal language if available, otherwise fallback to default formality'),
    ];
  }

  /**
   * Get supported tag handling as form options.
   */
  public function getSupportedTagHandling(): array {
    return [
      'off' => $this->t('off'),
      'xml' => $this->t('xml'),
      'html' => $this->t('html'),
    ];
  }

  /**
   * Get supported split sentences as form options.
   */
  public function getSupportedSplitSentences(): array {
    return [
      '0' => $this->t('No splitting at all, whole input is treated as one sentence'),
      '1' => $this->t('Splits on interpunction and on newlines (default)'),
      'nonewlines' => $this->t('Splits on interpunction only, ignoring newlines'),
    ];
  }

}
