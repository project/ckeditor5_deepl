<?php

namespace Drupal\ckeditor5_deepl\Plugin\KeyType;

use Drupal\ckeditor5_deepl\DeeplHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\key\Plugin\KeyTypeBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a generic key type for authentication.
 *
 * @KeyType(
 *   id = "deepl_api_key",
 *   label = @Translation("DeepL API Key"),
 *   description = @Translation("A free or pro DeepL API key."),
 *   group = "authentication",
 *   key_value = {
 *     "plugin" = "text_field"
 *   }
 * )
 */
class DeeplKeyType extends KeyTypeBase {

  /**
   * The Deepl helper.
   *
   * @var \Drupal\ckeditor5_deepl\DeeplHelper
   */
  protected DeeplHelper $deeplHelper;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DeeplHelper $deepl_helper) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->deeplHelper = $deepl_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('ckeditor5_deepl.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function generateKeyValue(array $configuration) {
    return \Drupal::service('password_generator')->generate(16);
  }

  /**
   * {@inheritdoc}
   */
  public function validateKeyValue(array $form, FormStateInterface $form_state, $key_value) {
    if (!empty($key_value)) {
      try {
        $this->deeplHelper->isValidDeeplKey($key_value);
      }
      catch (\Exception $e) {
        $form_state->setError($form['settings']['input_section']['key_input_settings']['key_value'], $e->getMessage());
      }
    }
  }

}
