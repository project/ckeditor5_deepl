<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_deepl\Plugin\CKEditor5Plugin;

use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableInterface;
use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableTrait;
use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;
use Drupal\ckeditor5\Plugin\CKEditor5PluginDefinition;
use Drupal\ckeditor5_deepl\DeeplHelper;
use Drupal\ckeditor5_deepl\DeeplKeys;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\editor\EditorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * CKEditor 5 OpenAI Completion plugin configuration.
 */
class DeeplIntegration extends CKEditor5PluginDefault implements ContainerFactoryPluginInterface, CKEditor5PluginConfigurableInterface {

  use CKEditor5PluginConfigurableTrait;

  /**
   * The Deepl helper service.
   *
   * @var \Drupal\ckeditor5_deepl\DeeplHelper
   */
  protected DeeplHelper $deeplHelper;

  /**
   * The Deepl keys service.
   *
   * @var \Drupal\ckeditor5_deepl\DeeplKeys
   */
  protected DeeplKeys $deeplKeys;

  /**
   * The deepl configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $deeplConifguration;

  /**
   * The default configuration for this plugin.
   *
   * @var string[][]
   */
  const DEFAULT_CONFIGURATION = [
    'deepl' => [
      'api_key' => '',
      'split_sentences' => '1',
      'show_formality' => FALSE,
      'formality' => 'default',
      'preserve_formatting' => FALSE,
      'tag_handling' => 'off',
      'source_languages' => [],
      'target_languages' => [],
    ],
  ];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return static::DEFAULT_CONFIGURATION;
  }

  /**
   * OpenAI CKEditor plugin constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param \Drupal\ckeditor5\Plugin\CKEditor5PluginDefinition $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\ckeditor5_deepl\DeeplHelper $deepl_helper
   *   The Deepl helper service.
   * @param \Drupal\ckeditor5_deepl\DeeplKeys $deepl_keys
   *   The Deepl keys service.
   * @param \Drupal\Core\Config\ImmutableConfig $deepl_conifguration
   *   The deepl configuration.
   */
  public function __construct(array $configuration, string $plugin_id, CKEditor5PluginDefinition $plugin_definition, DeeplHelper $deepl_helper, DeeplKeys $deepl_keys, ImmutableConfig $deepl_conifguration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->deeplHelper = $deepl_helper;
    $this->deeplKeys = $deepl_keys;
    $this->deeplConifguration = $deepl_conifguration;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('ckeditor5_deepl.helper'),
      $container->get('ckeditor5_deepl.keys'),
      $container->get('ckeditor5_deepl.config'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    if (!$this->deeplKeys->loadOneDeeplKey()) {
      $form['deepl'] = [
        '#markup' => $this->t('Add at least one Deepl API key to use this plugin. <a href=":link">Create a new key</a>.',
          [':link' => Url::fromRoute('entity.key.add_form')->toString()],
          ["context" => "ckeditor5_deepl"],
        ),
      ];
      return $form;
    }

    if ($this->configuration['deepl']['api_key']) {
      if (!$this->deeplHelper->isValidDeeplKey($this->deeplKeys->loadApiKey($this->configuration['deepl']['api_key']))) {
        $form['deepl'] = [
          '#markup' => $this->t('The Deepl API key is invalid. Please add a valid key.', [], ["context" => "ckeditor5_deepl"]),
        ];
        return $form;
      }
    }

    $form['deepl'] = [
      '#title' => $this->t('DeepL Settings', [], ["context" => "ckeditor5_deepl"]),
      '#description' => '<p>' . $this->t('Please have a look at the <a href="@url" target="_blank">DeepL API documentation</a> for more information on the settings listed below.',
        ['@url' => 'https://www.deepl.com/en/docs-api/'], ["context" => "ckeditor5_deepl"]) . '</p>',
      '#type' => 'details',
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    $form['deepl']['api_key'] = [
      '#type' => 'key_select',
      '#key_filters' => ['type' => 'deepl_api_key'],
      '#title' => $this->t('DeepL API key', [], ["context" => "ckeditor5_deepl"]),
      '#default_value' => $this->configuration['deepl']['api_key'] ?? '',
      '#description' => $this->t('DeepL API key for this editor format.', [], ["context" => "ckeditor5_deepl"]),
      '#required' => TRUE,
    ];

    $form['deepl']['split_sentences'] = [
      '#type' => 'select',
      '#title' => $this->t('Split sentences', [], ["context" => "ckeditor5_deepl"]),
      '#options' => $this->deeplHelper->getSupportedSplitSentences(),
      '#description' => $this->t('Sets whether the translation engine should first split the input into sentences.', [], ["context" => "ckeditor5_deepl"]),
      '#default_value' => $this->configuration['deepl']['split_sentences'] ?? $this->defaultConfiguration()['deepl']['split_sentences'],
    ];

    $form['deepl']['preserve_formatting'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Preserve formatting', [], ["context" => "ckeditor5_deepl"]),
      '#description' => $this->t('Sets whether the translation engine should respect the original formatting, even if it would usually correct some aspects.', [], ["context" => "ckeditor5_deepl"]),
      '#default_value' => $this->configuration['deepl']['preserve_formatting'] ?? $this->defaultConfiguration()['deepl']['preserve_formatting'],
    ];

    $form['deepl']['tag_handling'] = [
      '#type' => 'select',
      '#title' => $this->t('Tag handling', [], ["context" => "ckeditor5_deepl"]),
      '#description' => $this->t('Select the tag handling you want to use for translation.', [], ["context" => "ckeditor5_deepl"]),
      '#options' => $this->deeplHelper->getSupportedTagHandling(),
      '#default_value' => $this->configuration['deepl']['tag_handling'] ?? $this->defaultConfiguration()['deepl']['tag_handling'],
    ];

    $form['deepl']['formality'] = [
      '#type' => 'select',
      '#title' => $this->t('Formality', [], ["context" => "ckeditor5_deepl"]),
      '#description' => $this->t('Select the formality you want to use for translation.', [], ["context" => "ckeditor5_deepl"]),
      '#options' => $this->deeplHelper->getSupportedFormalities(),
      '#default_value' => $this->configuration['deepl']['formality'] ?? $this->defaultConfiguration()['deepl']['formality'],
    ];

    $form['deepl']['show_formality'] = [
      '#type' => 'checkbox',
      '#description' => $this->t('If checked, the formality selector will be shown in the editor. Otherwise, the default formality will be used.', [], ["context" => "ckeditor5_deepl"]),
      '#title' => $this->t('Show formality selector in editor', [], ["context" => "ckeditor5_deepl"]),
      '#default_value' => $this->configuration['deepl']['show_formality'] ?? FALSE,
    ];

    $form['deepl']['languages'] = [
      '#type' => 'details',
      '#title' => $this->t('Languages', [], ["context" => "ckeditor5_deepl"]),
      '#open' => FALSE,
    ];

    $source_languages = $this->deeplHelper->getSupportedRemoteLanguagesOptions('source');
    $form['deepl']['languages']['source_languages'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Source Languages', [], ["context" => "ckeditor5_deepl"]),
      '#description' => $this->t('Select the languages you want to make available as source language.', [], ["context" => "ckeditor5_deepl"]),
      '#options' => $source_languages,
      '#default_value' => !empty($this->configuration['deepl']['source_languages']) ? array_keys($this->configuration['deepl']['source_languages']) : array_keys($source_languages),
    ];

    $target_languages = $this->deeplHelper->getSupportedRemoteLanguagesOptions('target');
    $form['deepl']['languages']['target_languages'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Target Languages', [], ["context" => "ckeditor5_deepl"]),
      '#description' => $this->t('Select the languages you want to make available as target translation.', [], ["context" => "ckeditor5_deepl"]),
      '#options' => $target_languages,
      '#default_value' => !empty($this->configuration['deepl']['target_languages']) ? array_keys($this->configuration['deepl']['target_languages']) : array_keys($target_languages),
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $source_langs = $this->deeplHelper->getSupportedRemoteLanguagesOptions('source');
    foreach ($values['deepl']['languages']['source_languages'] as $lang) {
      if ($lang != 0) {
        $set_source_langs[$lang] = $source_langs[$lang];
      }
    }

    $target_langs = $this->deeplHelper->getSupportedRemoteLanguagesOptions('target');
    foreach ($values['deepl']['languages']['target_languages'] as $lang) {
      if ($lang != 0) {
        $set_target_langs[$lang] = $target_langs[$lang];
      }
    }

    $this->configuration['deepl']['api_key'] = $values['deepl']['api_key'];
    $this->configuration['deepl']['split_sentences'] = $values['deepl']['split_sentences'];
    $this->configuration['deepl']['show_formality'] = $values['deepl']['show_formality'];
    $this->configuration['deepl']['formality'] = $values['deepl']['formality'];
    $this->configuration['deepl']['preserve_formatting'] = $values['deepl']['preserve_formatting'];
    $this->configuration['deepl']['tag_handling'] = $values['deepl']['tag_handling'];
    $this->configuration['deepl']['source_languages'] = $set_source_langs;
    $this->configuration['deepl']['target_languages'] = $set_target_langs;
  }

  /**
   * {@inheritdoc}
   */
  public function getDynamicPluginConfig(array $static_plugin_config, EditorInterface $editor): array {
    $options = $static_plugin_config;
    $config = $this->getConfiguration();

    return [
      'ckeditor5_deepl_integration' => [
        'deepl' => [
          'show_formality' => $config['deepl']['show_formality'] ?? $options['deepl']['show_formality'],
          'formality' => $config['deepl']['formality'] ?? $options['deepl']['formality'],
          'source_languages' => $config['deepl']['source_languages'] ?? $options['deepl']['source_languages'],
          'target_languages' => $config['deepl']['target_languages'] ?? $options['deepl']['target_languages'],
        ],
      ],
    ];
  }

}
