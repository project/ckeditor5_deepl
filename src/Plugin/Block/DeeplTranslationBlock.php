<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_deepl\Plugin\Block;

use Drupal\ckeditor5_deepl\DeeplHelper;
use Drupal\ckeditor5_deepl\DeeplTranslator;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilder;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a deepl translation block.
 *
 * @Block(
 *   id = "ckeditor5_deepl_deepl_translation",
 *   admin_label = @Translation("Deepl Translation"),
 *   category = @Translation("Custom"),
 * )
 */
final class DeeplTranslationBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs the plugin instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly DeeplTranslator $deeplTranslator,
    private readonly DeeplHelper $deeplHelper,
    private readonly FormBuilder $formBuilder,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('ckeditor5_deepl.translator'),
      $container->get('ckeditor5_deepl.helper'),
      $container->get('form_builder'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'example' => $this->t('Hello world!', [], ["context" => "ckeditor5_deepl"]),
      'api_key' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form['api_key'] = [
      '#type' => 'key_select',
      '#key_filters' => ['type' => 'deepl_api_key'],
      '#title' => $this->t('DeepL API key', [], ["context" => "ckeditor5_deepl"]),
      '#default_value' => $this->configuration['api_key'] ?? '',
      '#description' => $this->t('DeepL API key for this block instance.', [], ["context" => "ckeditor5_deepl"]),
      '#required' => TRUE,
    ];

    $form['split_sentences'] = [
      '#type' => 'select',
      '#title' => $this->t('Split sentences', [], ["context" => "ckeditor5_deepl"]),
      '#options' => $this->deeplHelper->getSupportedSplitSentences(),
      '#description' => $this->t('Sets whether the translation engine should first split the input into sentences.', [], ["context" => "ckeditor5_deepl"]),
      '#default_value' => $this->configuration['split_sentences'] ?? 1,
    ];

    $form['preserve_formatting'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Preserve formatting'),
      '#description' => $this->t('Preserve the original formatting of the text.', [], ["context" => "ckeditor5_deepl"]),
      '#default_value' => $this->configuration['preserve_formatting'] ?? FALSE,
    ];

    $form['tag_handling'] = [
      '#type' => 'select',
      '#title' => $this->t('Tag handling', [], ["context" => "ckeditor5_deepl"]),
      '#description' => $this->t('Select the tag handling you want to use for translation.', [], ["context" => "ckeditor5_deepl"]),
      '#options' => $this->deeplHelper->getSupportedTagHandling(),
      '#default_value' => $this->configuration['tag_handling'] ?? 'off',
    ];

    $form['formality'] = [
      '#type' => 'select',
      '#title' => $this->t('Formality', [], ["context" => "ckeditor5_deepl"]),
      '#description' => $this->t('Select the formality you want to use for translation.', [], ["context" => "ckeditor5_deepl"]),
      '#options' => $this->deeplHelper->getSupportedFormalities(),
      '#default_value' => $this->configuration['formality'] ?? 'default',
    ];

    $form['show_formality'] = [
      '#type' => 'checkbox',
      '#description' => $this->t('If checked, the formality selector will be shown in the editor. Otherwise, the default formality will be used.', [], ["context" => "ckeditor5_deepl"]),
      '#title' => $this->t('Show formality selector in editor', [], ["context" => "ckeditor5_deepl"]),
      '#default_value' => $this->configuration['show_formality'] ?? FALSE,
    ];

    $form['languages'] = [
      '#type' => 'details',
      '#title' => $this->t('Languages', [], ["context" => "ckeditor5_deepl"]),
      '#open' => FALSE,
    ];

    $target_languages = $this->deeplHelper->getSupportedRemoteLanguagesOptions('target');
    $form['languages']['target_languages'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Target Languages', [], ["context" => "ckeditor5_deepl"]),
      '#description' => $this->t('Select the languages you want to make available as target translation.', [], ["context" => "ckeditor5_deepl"]),
      '#options' => $target_languages,
      '#default_value' => !empty($this->configuration['target_languages']) ? array_keys($this->configuration['target_languages']) : array_keys($target_languages),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();

    $target_langs = $this->deeplHelper->getSupportedRemoteLanguagesOptions('target');
    foreach ($values['languages']['target_languages'] as $lang) {
      if ($lang != 0) {
        $set_target_langs[$lang] = $target_langs[$lang];
      }
    }

    $this->configuration['api_key'] = $values['api_key'];
    $this->configuration['split_sentences'] = $values['split_sentences'];
    $this->configuration['show_formality'] = $values['show_formality'];
    $this->configuration['formality'] = $values['formality'];
    $this->configuration['preserve_formatting'] = $values['preserve_formatting'];
    $this->configuration['tag_handling'] = $values['tag_handling'];
    $this->configuration['target_languages'] = $set_target_langs;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build['form'] = $this->formBuilder->getForm('Drupal\ckeditor5_deepl\Form\DeeplTranslationForm', $this->getConfiguration());
    $build['#attributes']['class'][] = 'hidden';
    $build['#attached'] = [
      'library' => [
        'ckeditor5_deepl/deeplBlockIntegration',
      ],
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account): AccessResult {
    // @todo Evaluate the access condition here.
    return AccessResult::allowedIf(TRUE);
  }

}
