<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_deepl\Form;

use Drupal\ckeditor5_deepl\DeeplHelper;
use Drupal\ckeditor5_deepl\DeeplKeys;
use Drupal\ckeditor5_deepl\DeeplTranslator;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a CKEditor 5 DeepL form.
 */
final class DeeplTranslationForm extends FormBase {

  /**
   * The DeepL translator.
   *
   * @var \Drupal\ckeditor5_deepl\DeeplTranslator
   */
  protected $deeplTranslator;

  /**
   * The DeepL helper.
   *
   * @var \Drupal\ckeditor5_deepl\DeeplHelper
   */
  protected $deeplHelper;

  /**
   * The DeepL keys.
   *
   * @var \Drupal\ckeditor5_deepl\DeeplKeys
   */
  protected $deeplKeys;

  /**
   * The DeepL configuration from the block.
   *
   * @var array
   */
  protected $deeplConfig = [];

  /**
   * Constructs a new DeeplTranslationForm object.
   *
   * @param \Drupal\ckeditor5_deepl\DeeplTranslator $deeplTranslator
   *   The DeepL translator.
   * @param \Drupal\ckeditor5_deepl\DeeplHelper $deeplHelper
   *   The DeepL helper.
   * @param \Drupal\ckeditor5_deepl\DeeplKeys $deeplKeys
   *   The DeepL keys.
   */
  public function __construct(DeeplTranslator $deeplTranslator, DeeplHelper $deeplHelper, DeeplKeys $deeplKeys) {
    $this->deeplTranslator = $deeplTranslator;
    $this->deeplHelper = $deeplHelper;
    $this->deeplKeys = $deeplKeys;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('ckeditor5_deepl.translator'),
      $container->get('ckeditor5_deepl.helper'),
      $container->get('ckeditor5_deepl.keys'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'ckeditor5_deepl_deepl_tranlation';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $config = []): array {
    if (empty($this->deeplConfig) && !empty($config)) {
      $this->deeplConfig = $config;
    }

    $form['container'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'form-container'],
    ];

    $form['container']['text_orig'] = [
      '#type' => 'markup',
      '#markup' => '',
    ];

    $form['container']['text'] = [
      '#type' => 'textarea',
      '#format' => 'basic_html',
    ];

    $form['container']['target_language'] = [
      '#type' => 'select',
      '#title' => $this->t('Target language', [], ["context" => "ckeditor5_deepl"]),
      '#options' => $this->deeplConfig['target_languages'],
    ];

    if ($this->deeplConfig['show_formality']) {
      $form['container']['formality'] = [
        '#type' => 'select',
        '#title' => $this->t('Formality', [], ["context" => "ckeditor5_deepl"]),
        '#options' => $this->deeplHelper->getSupportedFormalities(),
      ];
    }

    $form['container']['submit'] = [
      '#type' => 'submit',
      '#ajax' => [
        'callback' => '::translateCallback',
        'wrapper' => 'form-container',
      ],
      '#value' => $this->t('Translate', [], ["context" => "ckeditor5_deepl"]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if (empty($form_state->getValue('text'))) {
      $form_state->setErrorByName('text', $this->t('Please insert some text.', [], ["context" => "ckeditor5_deepl"]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

  }

  /**
   * Ajax callback for the translation.
   */
  public function translateCallback(array &$form, FormStateInterface $form_state) {
    $element = $form['container'];

    if (empty($form_state->getErrors())) {
      $values = $form_state->getValues();

      $api_key = $this->deeplKeys->loadApiKey($this->deeplConfig['api_key']);
      $target_langcode = $values['target_language'];

      $options = [
        'split_sentences' => $this->deeplConfig['split_sentences'],
        'preserve_formatting' => $this->deeplConfig['preserve_formatting'],
        'tag_handling' => $this->deeplConfig['tag_handling'],
      ];

      // If it´s a free API key, the formality option is not available.
      // See https://support.deepl.com/hc/en-us/articles/4406433229330-The-formal-informal-feature-isn-t-displayed
      if (!$this->deeplTranslator->isAuthKeyFreeAccount($api_key) && !empty($values['formality'])) {
        $options['formality'] = $values['formality'];
      }

      $translation = $this->deeplTranslator->translate($api_key, $values['text'], NULL, $target_langcode, $options);

      $element['text_orig']['#title'] = $this->t('Original text', [], ["context" => "ckeditor5_deepl"]);
      $element['text_orig']['#markup'] = '<p>' . $values['text'] . '</p>';

      $element['text']['#attributes']['autofocus'] = "true";
      $element['text']['#value'] = $translation;
    }

    return $element;
  }

}
